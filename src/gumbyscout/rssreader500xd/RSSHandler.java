package gumbyscout.rssreader500xd;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RSSHandler extends DefaultHandler {
	private boolean inRssTitle;
	private boolean inTitle;
	private boolean inLink;
	private boolean inDescr;
	private boolean inItem;
	private StringBuilder bufferTitle;
	private StringBuilder bufferLink;
	private StringBuilder bufferDescr;
	private StringBuilder bufferRssTitle;
	private ArrayList<RSSItem> items;
	
	public RSSHandler(){
		//set booleans to false
		inTitle = false;
		inItem = false;
		inLink = false;
		inDescr = false;
		
		//initialize string builders
		bufferTitle = new StringBuilder();
		bufferLink = new StringBuilder();
		bufferDescr = new StringBuilder();
		bufferRssTitle = new StringBuilder();
		
		//create list
		items = new ArrayList<RSSItem>();
	}
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		//get title of RSS Feed
		if(localName.equalsIgnoreCase("title") && !inItem){
			inRssTitle = true;
			//empty last buffer
			bufferRssTitle.setLength(0);
		}
		else if(localName.equalsIgnoreCase("item")){
			inItem = true;
		}
		else if(inItem){
			if(localName.equalsIgnoreCase("title")){
				inTitle = true;
				bufferTitle.setLength(0);
			}
			else if(localName.equalsIgnoreCase("link")){
				inLink = true;
				bufferLink.setLength(0);
			}
			else if(localName.equalsIgnoreCase("description")){
				inDescr = true;
				bufferDescr.setLength(0);
			}
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		//get title of RSS Feed
		if(inRssTitle){
			bufferRssTitle.append(ch, start, length);
		}
		else if(inItem){
			if(inTitle){
				bufferTitle.append(ch, start, length);
			}
			else if(inLink){
				bufferLink.append(ch, start, length);
			}
			else if(inDescr){
				bufferDescr.append(ch, start, length);
			}
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		//get Title of RSS Feed
		if(localName.equalsIgnoreCase("title") && !inItem){
			inRssTitle = false;
		}
		else if(localName.equalsIgnoreCase("item")){
			inItem = false;

			//create and store the new RSSItem object
			RSSItem temp = new RSSItem();
			temp.setTitle(bufferTitle.toString());
			temp.setLink(bufferLink.toString());
			temp.setDescription(bufferDescr.toString());
			items.add(temp);
		}
		else if(inItem){
			if(localName.equalsIgnoreCase("title")){
				inTitle = false;
			}
			else if(localName.equalsIgnoreCase("link")){
				inLink = false;
			}
			else if(localName.equalsIgnoreCase("description")){
				inDescr = false;
			}
		}
	}
	
	public ArrayList<RSSItem> getItems(){
		return items;
	}
	
	public String getRssTitle(){
		return bufferRssTitle.toString();
	}

}
