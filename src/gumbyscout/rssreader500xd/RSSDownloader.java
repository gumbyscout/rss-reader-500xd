package gumbyscout.rssreader500xd;
import java.io.InputStream;
import java.net.URI;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;

public class RSSDownloader {
	public RSSHandler downloadRSSFeed(String url){
		//downloads and returns a handler with a parsed rss feed
		
		//make sure it starts with the right protocol
		if (!url.startsWith("http://") && !url.startsWith("https://")){
			   url = "http://" + url;
		}
		RSSHandler handler = null;
		try{
			URI uri = new URI(url);
			Downloader dl = new Downloader();
			dl.execute(uri);
			
			//return parsed handler
			handler = dl.get();	
		}
		catch(Exception e){
		}
		return handler;
	}
	
	class Downloader extends AsyncTask<URI, Void, RSSHandler> {

		@Override
		protected RSSHandler doInBackground(URI... params) {
			//create handler for parser
			RSSHandler handler = new RSSHandler();
			AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
			
			try{
				//create a http request
				HttpGet request = new HttpGet();
				URI uri = params[0];
				request.setURI(uri);
				
				//execute the http request
				HttpResponse response = client.execute(request);
				
				//if the file is found
				if(response.getStatusLine().getStatusCode() == 200){
					//create a parser
					SAXParserFactory spf = SAXParserFactory.newInstance();
					SAXParser sp = spf.newSAXParser();
					//get the file
					InputStream is = response.getEntity().getContent();
					//parse the file with the handler
					sp.parse(is, handler);
				}
			}
			catch(Exception e){
				
			}
			finally{
				//close the http client
				client.close();
			}
			return handler;
		}
		
	}

}
