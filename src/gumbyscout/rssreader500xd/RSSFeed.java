package gumbyscout.rssreader500xd;

import java.util.Calendar;

import android.os.Parcel;
import android.os.Parcelable;

public class RSSFeed implements Parcelable {
	private long id;
	private String name;
	private String url;
	private String date;
	
	public RSSFeed(){
		//default
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		//get calendar to get proper formated time for dat
		Calendar tdate = Calendar.getInstance();
		tdate.setTimeInMillis(Long.parseLong(date));
		String test = name + '\n' + url + '\n' + tdate.getTime().toString();
		return test;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		//write out important values 
		dest.writeLong(id);
		dest.writeString(name);
		dest.writeString(url);
		dest.writeString(date);
	}

	public static final Parcelable.Creator<RSSFeed> CREATOR = new Parcelable.Creator<RSSFeed>() {
		public RSSFeed createFromParcel(Parcel in) {
			return new RSSFeed(in);
		}

		public RSSFeed[] newArray(int size) {
			return new RSSFeed[size];
		}
	};

	private RSSFeed(Parcel in) {
		id = in.readLong();
		name = in.readString();
		url = in.readString();
		date = in.readString();
	}

}
