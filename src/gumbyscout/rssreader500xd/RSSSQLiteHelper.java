package gumbyscout.rssreader500xd;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class RSSSQLiteHelper extends SQLiteOpenHelper{
	private static final String DATABASE_NAME = "database.sqlite";
	private static final int DATABASE_VERSION = 1;
	
	public static final String FEED_TABLE = "Feeds";
	public static final String FEED_ID = "_id";
	public static final String FEED_NAME = "name";
	public static final String FEED_URL = "url";
	public static final String FEED_DATE = "date";
	
	public static final String[] FEED_COLUMNS = {
		FEED_ID,
		FEED_NAME,
		FEED_URL,
		FEED_DATE
	};
	
	public static final String ITEM_TABLE = "Items";
	public static final String ITEM_ID = "_id";
	public static final String ITEM_NAME = "name";
	public static final String ITEM_LINK = "link";
	public static final String ITEM_DESCR = "descr";
	public static final String ITEM_FEED_ID = "feed_id";
	
	public static final String[] ITEM_COLUMNS = {
		ITEM_ID,
		ITEM_NAME,
		ITEM_LINK,
		ITEM_DESCR,
		ITEM_FEED_ID
	};
	
	public RSSSQLiteHelper(Context context){
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		//might need this for foreign key support?
		db.execSQL("PRAGMA foreign_keys = ON;");
		
		//add feeds table
		String sql = "CREATE TABLE " + FEED_TABLE + " ("
				+ FEED_ID + " INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
				+ FEED_NAME + " TEXT NOT NULL,"
				+ FEED_URL + " TEXT NOT NULL,"
				+ FEED_DATE + " TEXT NOT NULL"
				+ ");";
		Log.i("SQL Feeds", sql);
		db.execSQL(sql);
		
		//add items table
		sql = "CREATE TABLE " + ITEM_TABLE + " ("
				+ ITEM_ID + " INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
				+ ITEM_NAME + " TEXT NOT NULL,"
				+ ITEM_LINK + " TEXT NOT NULL,"
				+ ITEM_DESCR + " TEXT,"
				+ ITEM_FEED_ID + " INTEGER NOT NULL,"
				+ "FOREIGN KEY (" + ITEM_FEED_ID + ") REFERENCES " + FEED_TABLE + " (" + FEED_ID + ")"
				+ ");";
		Log.i("SQL Items", sql);
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//had no reason to change this

	}

}
