package gumbyscout.rssreader500xd;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class RSSDbDataSource {
	private SQLiteDatabase database;
	private RSSSQLiteHelper databasehelper;
	private boolean isOpen;
	
	public RSSDbDataSource(Context context){
		databasehelper = new RSSSQLiteHelper(context);
		isOpen = false;
	}
	
	public void open(){
		//get a database object created by the helper
		database = databasehelper.getWritableDatabase();
		isOpen = true;
	}
	
	public boolean isOpen(){
		return isOpen;
	}
	
	public void close(){
		databasehelper.close();
		isOpen = false;
	}
	
	public void updateRSSFeedDate(RSSFeed feed){
		String sql = "UPDATE " + RSSSQLiteHelper.FEED_TABLE
				+ " SET " + RSSSQLiteHelper.FEED_DATE + "=" + feed.getDate()
				+ " WHERE " + RSSSQLiteHelper.FEED_ID + "=" + feed.getId();
		
		database.execSQL(sql);
	}
	
	public RSSFeed createRSSFeed(String name, String url){
		ContentValues values = new ContentValues();
		
		//put values in
		values.put(RSSSQLiteHelper.FEED_NAME, name);
		values.put(RSSSQLiteHelper.FEED_URL, url);
		
		//get date an insert as a string in miliseconds
		Calendar now = Calendar.getInstance();
		String date = now.getTimeInMillis() + "";
		Log.i("FEED_DATE", "Feed Date: " + date);
		values.put(RSSSQLiteHelper.FEED_DATE, date);
		
		//insert values into table, and get id to get object
		long id = database.insert(RSSSQLiteHelper.FEED_TABLE, null, values);
		
		//get cursor for object just added
		Cursor cursor = database.query(
				RSSSQLiteHelper.FEED_TABLE,
				RSSSQLiteHelper.FEED_COLUMNS,
				RSSSQLiteHelper.FEED_ID + "=" + id,
				null, null, null, null);
		cursor.moveToFirst();
		
		RSSFeed feed = cursorToRSSFeed(cursor);
		//close cursor
		cursor.close();
		return feed;
	}
	
	public RSSItem createRSSItem(String name, String link, String descr, long feedId){
		ContentValues values = new ContentValues();
		//put the values in
		values.put(RSSSQLiteHelper.ITEM_NAME, name);
		values.put(RSSSQLiteHelper.ITEM_LINK, link);
		values.put(RSSSQLiteHelper.ITEM_DESCR, descr);
		values.put(RSSSQLiteHelper.ITEM_FEED_ID, feedId);
		
		//insert values into table, and get id to get object
		long id = database.insert(RSSSQLiteHelper.ITEM_TABLE, null, values);
		
		//get the values of the data just put in the database, out of the database
		Cursor cursor = database.query(
				RSSSQLiteHelper.ITEM_TABLE,
				RSSSQLiteHelper.ITEM_COLUMNS,
				RSSSQLiteHelper.ITEM_ID + "=" + id,
				null, null, null, null);
		
		cursor.moveToFirst();
		RSSItem item = cursorToRSSItem(cursor);
		//don't forget to close cursor
		cursor.close();
		return item;
	}
	
	public ArrayList<RSSItem> createFromRSSItemList(ArrayList<RSSItem> items, long feedId){
		//drop old list items; a feed can only have one set of items
		String sql = "DELETE FROM " + RSSSQLiteHelper.ITEM_TABLE 
				+ " WHERE " + RSSSQLiteHelper.ITEM_FEED_ID + "=" + feedId;
		database.execSQL(sql);
		
		//adds a whole list at once, and makes sure each item has an parent id
		ArrayList<RSSItem> list = new ArrayList<RSSItem>();
		for(RSSItem item: items){
			String name = item.getTitle();
			String link = item.getLink();
			String descr = item.getDescription();
			RSSItem temp = createRSSItem(name, link, descr, feedId);
			list.add(temp);
		}
		return list;
	}
	
	private RSSItem cursorToRSSItem(Cursor cursor){
		//convenience function to get item from cursor 
		RSSItem item = new RSSItem();
		item.setId(cursor.getLong(0));
		item.setTitle(cursor.getString(1));
		item.setLink(cursor.getString(2));
		item.setDescription(cursor.getString(3));
		item.setFeedId(cursor.getLong(4));
		
		return item;
	}
	
	private RSSFeed cursorToRSSFeed(Cursor cursor){
		//convenience function to get feed from cursor
		RSSFeed feed = new RSSFeed();
		feed.setId(cursor.getLong(0));
		feed.setName(cursor.getString(1));
		feed.setUrl(cursor.getString(2));
		feed.setDate(cursor.getString(3));
		
		return feed;
	}
	
	public ArrayList<RSSItem> getRSSFeedItems(long feedId){
		ArrayList<RSSItem> items = new ArrayList<RSSItem>();
		
		//query the database for the items
		Cursor cursor = database.query(
				RSSSQLiteHelper.ITEM_TABLE,
				RSSSQLiteHelper.ITEM_COLUMNS,
				RSSSQLiteHelper.ITEM_FEED_ID + "=" + feedId,
				null, null, null, null);
		cursor.moveToFirst();
		
		//populate list
		while(!cursor.isAfterLast()){
			//get item at cursor
			RSSItem item = cursorToRSSItem(cursor);
			items.add(item);
			cursor.moveToNext();
		}
		//close cursor
		cursor.close();
		
		return items;
	}
	
	public ArrayList<RSSFeed> getRSSFeeds(){
		ArrayList<RSSFeed> feeds = new ArrayList<RSSFeed>();
		
		//query the database for all feeds
		Cursor cursor = database.query(
				RSSSQLiteHelper.FEED_TABLE,
				RSSSQLiteHelper.FEED_COLUMNS,
				null, null, null, null, null);
		cursor.moveToFirst();
		
		//populate list
		while(!cursor.isAfterLast()){
			RSSFeed feed= cursorToRSSFeed(cursor);
			feeds.add(feed);
			cursor.moveToNext();
		}
		//close cursor
		cursor.close();
		
		return feeds;
	}
	
	public void removeRSSFeed(long feedId){
		//remove a feed by feed id
		String sql = "DELETE FROM " + RSSSQLiteHelper.FEED_TABLE 
				+ " WHERE " + RSSSQLiteHelper.FEED_ID + "=" + feedId;
		database.execSQL(sql);
		//remove it's items
		sql = "DELETE FROM " + RSSSQLiteHelper.ITEM_TABLE
				+ " WHERE " + RSSSQLiteHelper.ITEM_FEED_ID + "=" + feedId;
		database.execSQL(sql);
	}

}
