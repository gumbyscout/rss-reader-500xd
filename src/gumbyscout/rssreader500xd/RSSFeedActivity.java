package gumbyscout.rssreader500xd;


import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class RSSFeedActivity extends ListActivity {
	public final static String FEED_KEY = "feed";
	public final static String ITEMS_KEY = "items";
	public final static String UPDATED_KEY = "updated";
	private ArrayList<RSSItem> items;
	private RSSFeed feed;
	private boolean isUpdated = false;
	private ArrayAdapter<RSSItem> adapter;
	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// get intent
		intent = getIntent();
		if (intent != null) {
			Bundle extras = intent.getExtras();
			feed = extras.getParcelable(FEED_KEY);

			// set activty title
			this.setTitle(feed.getName());

			// get items for feed
			items = extras.getParcelableArrayList(ITEMS_KEY);
			if (items == null) {
				items = new ArrayList<RSSItem>();
			}

			// set to adapter
			adapter = new ArrayAdapter<RSSItem>(getApplicationContext(),
					android.R.layout.simple_list_item_1, items);
			setListAdapter(adapter);

			// check if need to refresh
			isUpdated = refreshFeed();

		}

	}

	public boolean refreshFeed() {
		// get time for now
		Calendar now = Calendar.getInstance();
		// feed time to comparable number
		Calendar then = Calendar.getInstance();
		then.setTimeInMillis(Long.parseLong(feed.getDate())); // was stored as a
																// string

		int nowhour = now.get(Calendar.HOUR_OF_DAY);
		int thenhour = then.get(Calendar.HOUR_OF_DAY);
		int nowmin = now.get(Calendar.MINUTE);
		int thenmin = then.get(Calendar.MINUTE);

		if (nowhour > thenhour) {
			downloadFeed();
			return true;
		} else if (nowhour == 0 && thenhour == 23) {
			downloadFeed();
			return true;
		} else if (nowmin - thenmin > 30) {
			downloadFeed();
			return true;
		}
		return false;
	}

	private void downloadFeed() {
		RSSHandler handler = new RSSDownloader().downloadRSSFeed(feed.getUrl());
		if (handler != null) {
			items = handler.getItems();
			adapter.notifyDataSetChanged();
			isUpdated = true;
			
			//update feed's date
			Calendar now = Calendar.getInstance();
			String ndate = now.getTimeInMillis() + "";
			feed.setDate(ndate);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.rssfeed, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			onBackPressed();
			return true;
		} else if (item.getItemId() == R.id.action_refresh) {
			downloadFeed();
			isUpdated = true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		// put updated stuff in intent
		if(isUpdated){
			Log.i("FEEDACT", "Updated!");
			intent.putParcelableArrayListExtra(ITEMS_KEY, items);
			intent.putExtra(FEED_KEY, feed);
		}
		intent.putExtra(UPDATED_KEY, isUpdated);
		setResult(RESULT_OK, intent);
		finish();
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		//get item at position
		RSSItem item = items.get(position);
		
		//open alert
		AlertDialog.Builder articlePrompt = new AlertDialog.Builder(this);
	
		articlePrompt.setTitle(item.getTitle());
		
		//create webview to view description
		WebView wb = new WebView(this);
		wb.loadData(item.getDescription(), "text/html", null);
		articlePrompt.setView(wb);
		
		articlePrompt.setPositiveButton(R.string.label_ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						//do nothing!
					}
				});
		//create stupid object
		OpenUrlConfirm ouc = new OpenUrlConfirm();
		ouc.setRSSItem(item);
		articlePrompt.setNegativeButton(R.string.label_openurl, ouc);
		AlertDialog prompt = articlePrompt.create();
		prompt.show();
	}
	
	private class OpenUrlConfirm implements DialogInterface.OnClickListener{
		private RSSItem item;
		
		public void setRSSItem(RSSItem item){
			this.item = item;
		}
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			//open url
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(item.getLink())));	
		}
		
	}
}