package gumbyscout.rssreader500xd;

import android.os.Parcel;
import android.os.Parcelable;

public class RSSItem implements Parcelable {
	private long id;
	private String title;
	private String description;
	private String link;
	private long feedId;
	
	public RSSItem(){
		//default constructor
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setFeedId(long feedId) {
		this.feedId = feedId;
	}

	public long getFeedId() {
		return feedId;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return title;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(link);
		dest.writeLong(feedId);
	}

	public static final Parcelable.Creator<RSSItem> CREATOR = new Parcelable.Creator<RSSItem>() {
		public RSSItem createFromParcel(Parcel in) {
			return new RSSItem(in);
		}

		public RSSItem[] newArray(int size) {
			return new RSSItem[size];
		}
	};

	private RSSItem(Parcel in) {
		id = in.readLong();
		title = in.readString();
		description = in.readString();
		link = in.readString();
		feedId = in.readLong();
	}
}
