package gumbyscout.rssreader500xd;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends ListActivity {
	private RSSDbDataSource dataSource;
	private ArrayList<RSSFeed> feeds;
	private ArrayAdapter<RSSFeed> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//create database
		dataSource = new RSSDbDataSource(getApplicationContext());
		dataSource.open();
		
		
		//populate list view with feeds
		feeds = dataSource.getRSSFeeds();
		adapter = new ArrayAdapter<RSSFeed>(
					getApplicationContext(), android.R.layout.simple_list_item_1, feeds);
		setListAdapter(adapter);
		
		//register context menu
		ListView view = getListView();
        registerForContextMenu(view);
        
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(!dataSource.isOpen()){
			dataSource.open();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		dataSource.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if(item.getItemId() == R.id.action_addfeed){
			//inflate custom layout for alert box, needs final for arbitrary reason
			final View urlPromptView = this.getLayoutInflater().inflate(R.layout.addfeedlayout, null);
			
			//prompt for feed url
			AlertDialog.Builder urlPrompt = new AlertDialog.Builder(this);
			urlPrompt.setView(urlPromptView);
			urlPrompt.setTitle(R.string.title_addfeed);
			urlPrompt.setPositiveButton(R.string.label_ok,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							//create feed object and add to database
							EditText urlbox = (EditText) urlPromptView.findViewById(R.id.editText_enterurl);
							if(urlbox != null){
								Log.i("INFO", "Got url from editbox");
								String url = urlbox.getText() + "";
								//download the feed
								RSSHandler handler = new RSSDownloader().downloadRSSFeed(url);
								String name = handler.getRssTitle();
							
								//add to database, along with items
								RSSFeed feed = dataSource.createRSSFeed(name, url);
								dataSource.createFromRSSItemList(handler.getItems(), feed.getId());
							
								//add to and update adapter
								feeds.add(feed);
								adapter.notifyDataSetChanged();
							}
						}
					});
			urlPrompt.setNegativeButton(R.string.label_cancel,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							//cancelled do nothing
						}
					});
			AlertDialog prompt = urlPrompt.create();
			prompt.show();
		}
		else if(item.getItemId() == R.id.context_deletefeed){
			
			//get item info
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
			
			//confirm delete
			AlertDialog.Builder deletePrompt = new AlertDialog.Builder(this);
			deletePrompt.setMessage(R.string.prompt_deletefeed);
			//need to make a object to pass as listener, PIA
			DeleteConfirm dc = new DeleteConfirm();
			dc.setInfo(info);
			deletePrompt.setPositiveButton(R.string.label_ok, dc);
			deletePrompt.setNegativeButton(R.string.label_cancel,
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							//cancelled do nothing
						}
					});
			AlertDialog prompt = deletePrompt.create();
			prompt.show();
			
		}
		return false;
	}
	 
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		//get feed at location
		RSSFeed feed = feeds.get(position);
		
		//create intent and fill what's needed for activity
		Intent intent = new Intent(this, RSSFeedActivity.class);
		intent.putExtra(RSSFeedActivity.FEED_KEY, feed);
		ArrayList<RSSItem> items = dataSource.getRSSFeedItems(feed.getId());
		intent.putParcelableArrayListExtra(RSSFeedActivity.ITEMS_KEY, items);

		Log.i("INFO", "extras added");
	
		startActivityForResult(intent, 0);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode != RESULT_CANCELED){
			//get the bundle and update database if updated
			Bundle bundle = data.getExtras();
			
			boolean isUpdated = bundle.getBoolean(RSSFeedActivity.UPDATED_KEY);
			if(isUpdated){
				ArrayList<RSSItem> list = bundle.getParcelableArrayList(RSSFeedActivity.ITEMS_KEY);
				RSSFeed feed = bundle.getParcelable(RSSFeedActivity.FEED_KEY);
				if(!dataSource.isOpen()){
					dataSource.open();
				}
				dataSource.updateRSSFeedDate(feed);
				dataSource.createFromRSSItemList(list, feed.getId());
				
				//update list for changed feed times
				for(RSSFeed tfeed:feeds){
					if(tfeed.getId() == feed.getId()){
						int index = feeds.indexOf(tfeed);
						feeds.set(index, feed);
					}
				}
				adapter.notifyDataSetChanged();
			}
		}
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.main_context, menu);
	}
	
	private class DeleteConfirm implements DialogInterface.OnClickListener{
		//this class is to get around not being able to pass a variable passed to a function to 
		//an anonymous inner class
		private AdapterContextMenuInfo info;
		
		public void setInfo(AdapterContextMenuInfo info){
			this.info = info;
		}
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			Log.i("CONTEXT", "Inside delete");
			//get item at position
			RSSFeed feed = feeds.get(info.position);
			//remove it from list
			feeds.remove(info.position);
			//remove it from database
			dataSource.removeRSSFeed(feed.getId());
			//update adapter
			adapter.notifyDataSetChanged();
			
		}
		
	}
}
